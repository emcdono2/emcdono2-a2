package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
     
   
    @Test
    public void testTestSuiteRuns() {
			logger.info("Dummy test to show the test suite runs");
			assertTrue(true);
    }
    
    @Test
    public void testBoxComplete() {
    		DotsAndBoxesGrid test1 = new DotsAndBoxesGrid(4,4,2);
    		
			//No box
			assertFalse(test1.boxComplete(1,1));
			
			test1.drawHorizontal(1,1,1);
			test1.drawHorizontal(1,2,1);
			test1.drawVertical(1,1,1);
			test1.drawVertical(2,1,1);
			
			//Box is there
			assertTrue(test1.boxComplete(1,1));
    }

	 @Test
	 public void testDrawHorizontal() {
	 		DotsAndBoxesGrid test2 = new DotsAndBoxesGrid(4,4,2);
	 		
	 		test2.drawHorizontal(1,1,1);
	 		test2.drawHorizontal(1,1,1); //Should throw exception here
	 }
	 
	 @Test
	 public void testDrawVertical() {
	 		DotsAndBoxesGrid test3 = new DotsAndBoxesGrid(4,4,2);
	 		
	 		test3.drawVertical(1,1,1);
	 		test3.drawVertical(1,1,1); //Should throw exception here
	 }
}
